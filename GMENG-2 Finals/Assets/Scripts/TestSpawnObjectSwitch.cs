﻿using UnityEngine;
using System.Collections;

public class TestSpawnEvent : GameEvent
{
    public GameObject SpawnableObject;
}

public class TestSpawnObjectSwitch : MonoBehaviour
{
    public GameObject Spawnable;

    private TestSpawnEvent spawnEventDetails;

    void Start()
    {
        spawnEventDetails = new TestSpawnEvent();
        spawnEventDetails.SpawnableObject = Spawnable;
    }

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            Debug.Log(spawnEventDetails.SpawnableObject);
            this.RaiseEventGlobal<TestSpawnEvent>(spawnEventDetails);  
        }
    }
}
