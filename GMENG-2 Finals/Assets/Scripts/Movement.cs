﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    public float MovementSpeed;
    private Vector3 TargetPosition;
    void Start()
    {
        TargetPosition.x = 10;
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, TargetPosition, MovementSpeed *Time.deltaTime);

        if (transform.position.x >= TargetPosition.x)
        {
            Destroy(this.gameObject);
        }
    }
}
