﻿using UnityEngine;
using System.Collections;

public class Spawnable : MonoBehaviour {

    float i;
    public GameObject spawnRule;

    private DespawnEvent despawnEvent;
    // Use this for initialization
    void Start ()
    {
        despawnEvent = new DespawnEvent();
    }
    void OnDestroy()
    {
        Despawn();
    }
    void Despawn()
    {
        spawnRule.RaiseEvent<DespawnEvent>(despawnEvent);
    }
}
