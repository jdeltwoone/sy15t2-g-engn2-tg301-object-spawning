﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SpawnRuleSpecific : SpawnRule
{
    public Vector3 SpecifiedArea;
    public int SpawnableLimit;

    private int spawnedObject;
    private Vector3 startPosition;
    private Transform spawnTransform;
    private int spawnablesElement;
    private bool nextObject;

    void Start()
    {
        spawnTransform = GetComponent<Transform>();
        EventDetails = new SpawnEvent();
        startPosition = spawnTransform.position;
        nextObject = true;
    }
    void Update()
    {
        spawnedObject = SpawnedObjects.Count;
        spawnTransform.position = SpecifiedArea;
        if (nextObject && spawnedObject < SpawnableLimit)
        {
            StartCoroutine(Spawn(SpawnInterval));
        }
        SpawnedObjects.Remove(null);
    }

    IEnumerator Spawn(float interval)
    {
        nextObject = false;
        spawnablesElement = Random.Range(0, Spawnables.Count);
        SpawnObject(Spawnables[spawnablesElement]);
        yield return new WaitForSeconds(interval);
        nextObject = true;
    }

}
