﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Wave
{
    public Spawnable ObjectPrefab;
    public int ObjectsPerWave;
    public float SpawningInterval;
}

public class SpawnRuleWaves : SpawnRule
{
    public List<Wave> SpawnableWaves;
    public bool InfiniteWaves;
    public bool OverlappingWaves;
    public float WaveSpawningInterval;

    private int currentWave;
    private bool nextWave;

    private int testListCount;

    void Start()
    {
        nextWave = true;
        EventDetails = new SpawnEvent();
    }

    void Update()
    {
        if (nextWave && WaveSpawningInterval != 0)
        {
            if (InfiniteWaves && currentWave == SpawnableWaves.Count)
            {
                currentWave = 0;
            }
            StartCoroutine(SpawnWave(SpawnableWaves[currentWave]));
            currentWave++;
            SpawnedObjects.Remove(null);

            if (!InfiniteWaves && currentWave >= SpawnableWaves.Count)
            {
                StopAllCoroutines();
            }
        }
        testListCount = SpawnableWaves.Count;
    }
    IEnumerator SpawnWave(Wave wave)
    {
        nextWave = false;
        if (OverlappingWaves)
        {
            yield return new WaitForSeconds(WaveSpawningInterval);
            nextWave = true;
        }
        int counter = 0;
        while (counter < wave.ObjectsPerWave)
        {
            counter++;
            EventDetails.SpawnableObject = wave.ObjectPrefab;
            SpawnObject(wave.ObjectPrefab);
            yield return new WaitForSeconds(wave.SpawningInterval);
        }
        yield return new WaitForSeconds(WaveSpawningInterval);
        nextWave = true;
    }

    public void SkipWave()
    {
        if (currentWave < (SpawnableWaves.Count - 1))
        {
            currentWave += 1;
        }
        else if (InfiniteWaves == true)
        {
            currentWave = 0;
        }
        StopAllCoroutines();
        StartCoroutine(SpawnWave(SpawnableWaves[currentWave]));
    }
}
