﻿using UnityEngine;
using System.Collections;

public class KillTimer : MonoBehaviour
{
    private float DeathTimer;
    void Update()
    {
        DeathTimer += Time.deltaTime;
        if (DeathTimer >= 5)
        {
            Destroy(this.gameObject);
        }
    }
}
