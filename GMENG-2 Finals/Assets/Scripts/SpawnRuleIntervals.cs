﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnRuleIntervals : SpawnRule
{
    public bool RandomOrder;
    public bool InfiniteSpawning;

    private bool nextObject;
    private int spawnablesElement;
    private int objectsSpawnedCount;
    void Start()
    {
        spawnablesElement = 0;
        nextObject = true;
        EventDetails = new SpawnEvent();
    }
    void Update()
    {
        if (nextObject && SpawnInterval != 0)
        {
            if (InfiniteSpawning && spawnablesElement >= Spawnables.Count)
            {
                spawnablesElement = 0;
            }
            StartCoroutine(SpawnObject(SpawnInterval));
            if (!InfiniteSpawning && objectsSpawnedCount >= Spawnables.Count)
            {
                StopAllCoroutines();
            }
        }
        SpawnedObjects.Remove(null);
    }
    IEnumerator SpawnObject(float interval)
    {
        nextObject = false;
        if (RandomOrder)
        {
            spawnablesElement = Random.Range(0, Spawnables.Count);
        }
        SpawnObject(Spawnables[spawnablesElement]);
        spawnablesElement++;
        objectsSpawnedCount++;
        yield return new WaitForSeconds(interval);
        nextObject = true;
    }

    public void ManualSpawn()
    {
        if (SpawnInterval == 0)
        {
            nextObject = false;
            spawnablesElement = Random.Range(0, Spawnables.Count);
            SpawnObject(Spawnables[spawnablesElement]);
            objectsSpawnedCount++;
            nextObject = true;
        }
    }
}
