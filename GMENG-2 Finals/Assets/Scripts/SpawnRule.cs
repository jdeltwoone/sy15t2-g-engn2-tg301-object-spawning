﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class SpawnRule : MonoBehaviour
{
    public List<Spawnable> Spawnables;
    [HideInInspector]public List<Spawnable> SpawnedObjects;
    public float SpawnInterval;

    protected SpawnEvent EventDetails;

    void OnEnable()
    {
        this.AddEventListener<DespawnEvent>(RemoveFromList);
    }

    void onDisable()
    {
        this.RemoveEventListener<DespawnEvent>(RemoveFromList);
    }

    public void AddToList(Spawnable spawnedObject)
    {
        SpawnedObjects.Add(spawnedObject);
    }
    public void RemoveFromList(DespawnEvent despawnEvent)
    {
        SpawnedObjects.Remove(despawnEvent.DespawnableObject);
    }
    public void SpawnObject(Spawnable spawnableObject)
    {
        Spawnable clone = (Spawnable)Instantiate(spawnableObject);
        clone.transform.position = this.transform.position;
        clone.spawnRule = this.gameObject;
        AddToList(clone);
    }
}
