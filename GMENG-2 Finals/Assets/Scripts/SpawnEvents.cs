﻿using UnityEngine;
using System.Collections;

public class SpawnEvent : GameEvent
{
    public Spawnable SpawnableObject;
}

public class DespawnEvent : GameEvent
{
    public Spawnable DespawnableObject;
}