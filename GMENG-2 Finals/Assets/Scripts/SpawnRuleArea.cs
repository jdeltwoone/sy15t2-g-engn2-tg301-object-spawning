﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnRuleArea : SpawnRule
{

    public float SpawnRadius;
    public int SpawnableLimit;

    private int spawnedObject;
    private Vector3 startPosition;
    private Transform spawnTransform;
    private int spawnablesElement;
    private bool nextObject;

   
    void Start()
    {
        spawnTransform = GetComponent<Transform>();
        EventDetails = new SpawnEvent();
        startPosition = spawnTransform.position;
        nextObject = true;
    }
    void Update()
    {
        float areaX;
        float areaY;
        
        if (nextObject && spawnedObject < SpawnableLimit)
        {
            areaX = Random.Range(-SpawnRadius / 2, SpawnRadius / 2);
            areaY = Random.Range(-SpawnRadius / 2, SpawnRadius / 2);

            spawnedObject = SpawnedObjects.Count;
            spawnTransform.position = new Vector3(startPosition.x + areaX, startPosition.y + areaY);
            StartCoroutine(Spawn(SpawnInterval));
        }
        SpawnedObjects.Remove(null);
    }
    IEnumerator Spawn(float interval)
    {
        nextObject = false;
        spawnablesElement = Random.Range(0, Spawnables.Count);
        SpawnObject(Spawnables[spawnablesElement]);
        yield return new WaitForSeconds(interval);
        nextObject = true;
    }
}
